# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-03-11
### Added
- ScriptModificationProcessor class
- ScriptGenerator utility class
- ScriptFileSystem utility class
- Script Templates files
- Unity Package files
- CHANGELOG
- README
- Initial commit

[Unreleased]: https://bitbucket.org/nostgameteam/script-generator/branches/compare/master%0D1.0.0
[1.0.0]: https://bitbucket.org/nostgameteam/script-generator/src/1.0.0/