using UnityEngine;

namespace #NAMESPACE#
{
	[CreateAssetMenu(fileName = "#SCRIPTNAME#", menuName = "#NAMESPACE#/#SCRIPTNAME#", order = 110)]
	public sealed class #SCRIPTNAME# : ScriptableObject 
	{
		#NOTRIM#
	}
}