using UnityEditor;
using UnityEngine;

namespace #NAMESPACE#.Editor
{
    // Remove the sufix 'Editor' from the type below
    //[CustomEditor(typeof(#SCRIPTNAME#))]
    public sealed class #SCRIPTNAME# : Editor
    {
    	//private #SCRIPTNAME# _#SCRIPTNAME_LOWER#;
    	
        private void OnEnable()
        {
            // Remove the sufix 'Editor' from the type below
        	//_#SCRIPTNAME_LOWER# = (#SCRIPTNAME#) target;
        }

        // Draws variables into Inspector View
    	public override void OnInspectorGUI()
        {
        	base.OnInspectorGUI();
        	EditorGUILayout.Space();
        }

        // Draws points, lines, meshes, handles etc into Scene View
        private void OnSceneGUI()
        {
        }
    }
}