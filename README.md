# Script Generator

* File generator for Unity scripts
* Unity minimum version: **2018.1**
* Current version: **1.0.0**
* License: **MIT**

## Summary

Use scripts templates with smart namespace creation.

![alt text][script-templates-on-inspector]

## How To Use

1. Copy and paste all scripts template files from [/ScriptTemplates~](/ScriptTemplates~/) to `<unity-hub-folder>\Hub\Editor\<version>\Editor\Data\Resources\ScriptTemplates`;
2. Restart Unity;
3. If you didn't set your project root namespace, go to **Edit > Project Settings > Editor > C# Project Generation, Root namespace** and set one;
4. Create any Script using the creation menu.

## Installation

### Using the Package Registry Server

Follow the instructions inside [here](https://cutt.ly/ukvj1c8) and the package **ActionCode-Script Generator** 
will be available for you to install using the **Package Manager** windows.

### Using the Git URL

You will need a **Git client** installed on your computer with the Path variable already set. 

Use the **Package Manager** "Add package from git URL..." feature or add manually this line inside `dependencies` attribute: 

```json
"com.actioncode.script-generator":"https://bitbucket.org/nostgameteam/script-generator.git"
```

---

**Hyago Oliveira**

[BitBucket](https://bitbucket.org/HyagoGow/) -
[Unity Connect](https://connect.unity.com/u/hyago-oliveira) -
<hyagogow@gmail.com>

[script-templates-on-inspector]: /Documentation~/script-templates-on-inspector.jpg "Using Script Templates"