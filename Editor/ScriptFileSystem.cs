﻿using System.IO;
using UnityEditor;

namespace ActionCode.ScriptGenerator.Editor
{
    /// <summary>
    /// Utility class to handle script files.
    /// </summary>
    public static class ScriptFileSystem
    {
        /// <summary>
        /// Path separator used by Unity AssetDatabase API.
        /// </summary>
        public const char UNITY_PATH_SEPARATOR = '/';

        /// <summary>
        /// Checks if the given path is from any Unity package.
        /// </summary>
        /// <param name="path">The path to test. Can be Assets/Script/ or Assets\\Script\\</param>
        /// <returns>Whether the oath is from a package.</returns>
        public static bool IsFromPackage(string path)
        {
            const string unityPackagePathPrefix = "Packages/";
            const string systemPackagePathPrefix = "Packages\\";
            return
                path.StartsWith(unityPackagePathPrefix) ||
                path.StartsWith(systemPackagePathPrefix);
        }

        /// <summary>
        /// Removes all separators from the given folder.
        /// </summary>
        /// <param name="path">A path.</param>
        /// <param name="separator">The new separator.</param>
        /// <returns>A string with the new separator applied.</returns>
        public static string ReplaceAllFolderSeparators(string path, char separator)
        {
            path = path.
                Replace(Path.DirectorySeparatorChar, separator).
                Replace(Path.AltDirectorySeparatorChar, separator);

            var needFinalSeparator = !Path.HasExtension(path) && !path.EndsWith(separator.ToString());
            if (needFinalSeparator) path += separator;

            return path;
        }

        /// <summary>
        /// Removes the given folder from the path.
        /// </summary>
        /// <param name="path">A path.</param>
        /// <param name="folders">Folders to remove.</param>
        /// <returns>A new path without the folders.</returns>
        public static string RemoveFolders(string path, params string[] folders)
        {
            path = ReplaceAllFolderSeparators(path, UNITY_PATH_SEPARATOR);
            foreach (var folder in folders)
            {
                var folderWithSeparator = ReplaceAllFolderSeparators(folder, UNITY_PATH_SEPARATOR);
                path = path.Replace(folderWithSeparator, string.Empty);
            }

            return path;
        }

        /// <summary>
        /// Gets the path from currently selected asset inside Project windows.
        /// </summary>
        /// <returns></returns>
        public static string GetSelectionFolder()
        {
            var assetPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            var isFile = File.Exists(assetPath);
            var path = isFile ? Path.GetDirectoryName(assetPath) : assetPath;

            return path.Replace(Path.DirectorySeparatorChar, UNITY_PATH_SEPARATOR);
        }
    }
}
