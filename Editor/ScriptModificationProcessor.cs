﻿namespace ActionCode.ScriptGenerator.Editor
{
    /// <summary>
    /// Modification processor to Script files.
    /// <para>Adds the namespace set on Project Settings > Editor > C# Generation, Root namespace to a new created script.</para>
    /// </summary>
    public sealed class ScriptModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        public static void OnWillCreateAsset(string metaFilePath)
            => ScriptGenerator.ReplaceCSharpNamespace(metaFilePath);
    }
}
