﻿using System.IO;
using UnityEditor;

namespace ActionCode.ScriptGenerator.Editor
{
    /// <summary>
    /// Utility class to generate script templates.
    /// </summary>
    public static class ScriptGenerator
    {
        public const char NAMESPACE_DOT = '.';
        public const string SCRIPT_EXTENSION = ".cs";
        public const string SCRIPT_NAME = "#SCRIPTNAME#";
        public const string NAMESPACE_KEY = "#NAMESPACE#";

        /// <summary>
        /// Replaces the namespace from the file with the given meta file path.
        /// </summary>
        /// <param name="metaFilePath">The script meta file path. It'll have a '.meta' extension.</param>
        public static void ReplaceCSharpNamespace(string metaFilePath)
        {
            // If metaFilePath is from a script file, it'd be something like "Assets/Scripts/NewScript.cs.meta"
            var invalidFile = !IsCSharpFile(metaFilePath, out string scriptNameWithExtension);
            if (invalidFile) return;

            var directoryPath = Path.GetDirectoryName(metaFilePath);
            var scriptPath = Path.Combine(directoryPath, scriptNameWithExtension);
            var scriptContent = File.ReadAllText(scriptPath);

            var @namespace = GetNamespace(directoryPath);
            scriptContent = scriptContent.Replace(NAMESPACE_KEY, @namespace);

            File.WriteAllText(scriptPath, scriptContent);
            AssetDatabase.Refresh();
        }

        /// <summary>
        /// Returns the a fully namespace for the given path.
        /// </summary>
        /// <param name="directoryPath">A directory path.</param>
        /// <returns>A string namespace i.e. {ProjectNamespace}.{ScriptFolder}</returns>
        public static string GetNamespace(string directoryPath)
        {
            var rootNamespace = GetRootNamespace();
            var isFromPackage = ScriptFileSystem.IsFromPackage(directoryPath);
            var childNamespace = isFromPackage ? "YourPackage" : GetChildNamespace(directoryPath);

            return $"{rootNamespace}.{childNamespace}".Trim(NAMESPACE_DOT);
        }

        /// <summary>
        /// Returns the project root namespace set using Project Settings > C# Project Generation, Root namespace.
        /// </summary>
        /// <returns></returns>
        public static string GetRootNamespace()
            => EditorSettings.projectGenerationRootNamespace;

        /// <summary>
        /// Returns a child namespace from the given file.
        /// <para>Assets/Scripts/Player/Character.cs will return simply Player.</para>
        /// </summary>
        /// <param name="path">A path.</param>
        /// <returns></returns>
        public static string GetChildNamespace(string path)
        {
            var childNamespace = RemoveInvalidFolders(path);
            return ScriptFileSystem.ReplaceAllFolderSeparators(childNamespace, NAMESPACE_DOT);
        }

        /// <summary>
        /// Checks if the given meta path is from a C# script file.
        /// </summary>
        /// <param name="metaFilePath">The script meta file path. It'll have a '.meta' extension.</param>
        /// <param name="fileName">The filename with extension.</param>
        /// <returns></returns>
        public static bool IsCSharpFile(string metaFilePath, out string fileName)
        {
            fileName = Path.GetFileNameWithoutExtension(metaFilePath);
            return fileName.EndsWith(SCRIPT_EXTENSION);
        }

        private static string RemoveInvalidFolders(string path)
        {
            var invalidFolders = new string[] { "Assets", "Scripts", "Packages", "Runtime", "Editor" };
            return ScriptFileSystem.RemoveFolders(path, invalidFolders);
        }
    }
}
